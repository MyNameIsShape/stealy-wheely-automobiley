fs = require 'fs'

module.exports = {}
module.exports.fs = fs
module.exports.get = ()->
  scorelist = []
  scores = fs.readFileSync 'scores.sc', {encoding: 'utf8'}
  if scores.charAt scores.length-1 is ';'
    scores = scores.substring(0,scores.length-1)
  hs = scores.split ';'
  for sid of hs
    scorer = hs[sid].split ':'
    score_object = 
      name: scorer[0]
      score: scorer[1]
    scorelist.push(score_object)
  return scorelist
module.exports.set = (scorer)->
  scores = @get()
  si = 0
  while si < scores.length
    if Number(scores[si].score) < Number(scorer.score)
      sc = scores[si]
      replace_string = "#{sc.name}:#{sc.score};"
      scorer_string = "#{scorer.name}:#{scorer.score};"
      highscores = fs.readFileSync 'scores.sc', {encoding: 'utf8'}
      highscores = highscores.replace(replace_string, scorer_string)
      fs.writeFileSync('scores.sc', highscores, 'utf8')
      scorer = sc
    si++