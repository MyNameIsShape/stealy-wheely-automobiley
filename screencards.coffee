module.exports = {}
module.exports.title = (game, engine, factory, chalk)->
  engine.terminal.cls()
  engine.terminal.newline()
  engine.terminal.write(chalk.greenBright("   Stealy Wheely Automobiley"))
  engine.terminal.writeline("\n     A.K.A. birthday card for Christian.")
  engine.terminal.newline()
  engine.terminal.write("      v0.0.1")
  engine.terminal.newline()
  engine.terminal.newline()
  engine.terminal.newline()
  engine.terminal.write("  (1) New Game")
  engine.terminal.newline()
  engine.terminal.writeline "  (2) High Scores"
  engine.terminal.writeline ""
  engine.terminal.writeline(chalk.blueBright("  Make a selection."))
  
  game.handlers.keys["1"] = (()->
      @this['intro'](@game, @engine, @factory, @chalk)
    ).bind({this: @, game: game, engine: engine, factory: factory, chalk: chalk})
  
  game.handlers.keys[2] = (()->
      game.handlers.keys = {}
      @game.highscores()
    ).bind({this: @, game: game, engine: engine, factory: factory, chalk: chalk})

module.exports.intro = (game, engine, factory, chalk)->
  
  game.handlers.keys = {}  
  story = new factory.Story
    randomize: true
  engine.terminal.cls()
  for line_id of story.story
    line = story.story[line_id]
    stylings = 
      ":green:": "greenBright"
      ":greenBright:": "greenBright"
      ":red:": "redBright"
      ":redBright:": "redBright"
      ":magenta:": "magenta"
      ":cyan:": "cyan"
    for styling_id of stylings  
      if line.includes styling_id
        line = chalk[stylings[styling_id]](line.replace(styling_id,""))
    engine.terminal.write line
    engine.terminal.newline()
        
  engine.terminal.newline()
  engine.terminal.write ["     ", chalk.greenBright("( C )"), "ontinue"].join ''
  game.handlers.keys["c"] = (()->
      @game.start_new()
      return
    ).bind {game: game}
  return
      