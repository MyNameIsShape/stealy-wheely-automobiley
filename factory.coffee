module.exports = {}

module.exports.NewMapName = ()->
  name = ""
  adds = ["Washington", "Jones", "Hill", "Coronado", "Sand", "Ocean", "Breezy", "Starry", "Grassy", "Windy", "Shady", "Camp", "Police"]
  add_to = Math.floor(Math.random()*adds.length)
  name = "#{adds[add_to]}"
  adds = ["Hills", "Heights", "Valley", "View", "-view", "-view", "Springs", "Lake", "Lakes", "Creek", "Plaza", "Woods", "-wood"]
  add_to = Math.floor(Math.random()*adds.length)
  name = "#{name} #{adds[add_to]}"
  if name.indexOf " -" > -1
    name = name.replace " -", ""
  return name

module.exports.GenerateString = (type)->
  if type is "building name"
    cakeplace = Math.random()*100
    if cakeplace < 8
      names = ['Help Wanted', 'Chef Needed', 'Accepting Applications']
      return names[Math.floor(Math.random()*names.length)]
    if cakeplace > 95
      names = ["Cakes!!", "Cake Kitchen", "Birthday Cake Kitchen", "Ice Cream and Cake"]
      return names[Math.floor(Math.random()*names.length)]
    set = Math.floor(Math.random()*2)
    if Math.random()*100 > 75
      return "Costume Shop"
    if set is 0
      names = ["BurgerBash", "Ron's Auto Insurance", "Boxes 'r we's", "Jack's Chicken", "Big Mama BBQ", "Papa T's Buffet"]
      push_names = ["Hardware Store", "Loose Screws", "Sports and Things", "Nigel's Sporting Goods"]
      for pp of push_names
        names.push push_names[pp]
      return names[Math.floor(Math.random()*names.length)]
    else if set is 1
      name = []
      name_parts = ["Sport", "Sports", "Tools", "Hardware", "Costumes and", "House of", "Burger", "Chicken", "Tech", "Computer", "Value", "Best", "Old", "Fat Ted's", "Sai-gon", "Thai", "Hugs"]
      name_parts.push 'Cook\'s', "Brian's", "Tom's", "We", "We're"
      name.push name_parts[Math.floor(Math.random()*name_parts.length)]
      name_parts = ["and Disguises", "Burgers", "Shack", "Hut", "Hardware", "Repair", "Noodles", "Value", "Village", "Buy", "Thrift", "Antiques", "Collectibles", "Food", "Pancakes", "Store", "Store", "Store", "-Depot", "-Spot", "Fried Chicken", "Chicken", "Police", "Cops", "Not Cops", "Wanted"]
      name_parts.push '-Stop', '-Hub'
      name.push name_parts[Math.floor(Math.random()*name_parts.length)]
      name = name.join ' '
      if name.indexOf " -" > -1
        name = name.replace " -", ""
      return name

module.exports.ClassifyString = (str)->
  str = str.toLowerCase()
  types =
    'job': ['help', 'applications', 'chef needed']
    'birthday': ['cake', 'kitchen', 'ice cream','village','value']
    'sports': ['sport']
    'food': ['burger', 'chicken', 'sai-gon', 'thai', 'burgers', 'buffet']
    'tools': ['hardware', 'tools', 'screws', 'repair']
    'disguise': ['costume', 'disguise','thrift']
    'fence': ['tech', 'value', 'village', 'antiques', 'collectibles', 'thrift', 'repair']
  typed = false
  for t of types
    tp = types[t]
    for tag_id of tp
      tag = tp[tag_id]
      if str.includes tag
        return t
  return 'stuff'
        
      

class module.exports.Map
  constructor: (params)->
    @map = {}
    @width = 10
    @height = 10
    @solids = "X"
    @tiles = "."
    @entities = {}
    @actions = {}
    sale_prices = [90,90,90,90,90,90,90,100,100,100,100,100,100,100,100,100,110,115,120,150,200,225]
    @car_sale_price = sale_prices[Math.floor(Math.random()*sale_prices.length)]
    @car_sale_bonus = Math.round(@car_sale_price * 0.25)+Math.floor(Math.random()*50)
    for parameter_key of params
      @[parameter_key] = params[parameter_key]
    @initializeMap = ()->
      mx = 0
      my = 0
      while my < @height
        coords = ["x", mx.toString(), "y", my.toString()].join ''
        @map[coords] = @tiles.substring(0,1)
        mx++
        if mx >= @width
          mx = 0
          my++
      return
    return @

class module.exports.Entity
  constructor: (params)->
    @name = "npc"
    @tile = "@"
    @styling = (str)->return str
    @visible = true
    @countdown = 8
    @worldX = 0
    @worldY = 0
    @xprevious = 0
    @yprevious = 0
    @zprevious = 0
    @x = 0
    @y = 0
    @z = 0
    @actions = {}
    @solid = true
    @lvl = 1
    @exp = 0
    @expNext = 25
    @strength = 1
    @speed = 1
    @smarts = 1
    @luck = 0
    @hpMax = 5
    @hp = 5
    @wanted = 0
    @cash = 0
    @type = "person"
    @equipment =
      weapon: 'none'
      armor: 'none'
      tool: 'none'
    for parameter_key of params
      @[parameter_key] = params[parameter_key]

module.exports.Story = (params)->
  @randomize = params.randomize ?= false
  @person = params.person ?= "you"
  @reason = params.reason ?= "debt"
  @subreason = params.subreason ?= "gambling"
  @total = params.total ?= 3000
  
  @story = [""
            "Uh oh."
            "reason"
             "subreason"
             "\n\n"
             "You hoped you'd never have to do this again..."
             ":greenBright:   But, you need some QUICK CASH."
             "\n\n    It's time for...\n"
             "    STEALY WHEELY AUTOMOBILEY"]
  
  if @randomize is true
    peopleList = ['you', 'you', 'you', 'you', 'you', 'brother','sister','uncle','aunt','dad','mom','grandpa','grandma','girlfriend','boyfriend']
    personKey = Math.floor(Math.random()*peopleList.length)
    @person = peopleList[personKey]
    reasonList = ['debt', 'debt', 'debt', 'debt', 'sick', 'shakedown']
    reasonKey = Math.floor(Math.random()*reasonList.length)
    @reason = reasonList[reasonKey]
    subreasonList = ['debt']
    subreasonKey = Math.floor(Math.random()*subreasonList.length)
    @subreason = subreasonList[reasonKey]
      
  @story[1] = "Uh oh."
  @story[2] = ":greenBright:"
  storyline = "You"
  havehas = "have"
  if @person != 'you'
    storyline = [storyline, 'r ', @person, " "].join ''
    havehas = "has"
  else
    storyline = [storyline, ""].join ''
  
  storyline = [storyline, ' ', havehas, ' '].join ''
    
  storyswap =
    debt: "run up $"+@total+" in DEBT."
    sick: "been diagnosed with DEADLY DYING DISEASE\n   and needs $"+@total+" to pay for treatment."
    shakedown: "had it!!\n  If you don't pay them back the $"+@total+"\n    you owe them, they're going to\n      kill you."
  
  subreasons = 
    debt: ["gambling", "horse racing", "poker", "bar tab"]
    sick: ["die","constipation","body odor", "hair loss"]
    shakedown: ["..."]
  
  if @randomize is true
    subreasonList = subreasons[@reason]
    @subreason = subreasonList[Math.floor(Math.random()*subreasonList.length)]
  
  @story[2] = [@story[2], storyline, storyswap[@reason]].join ''
  switch @subreason
    when 'gambling' then @story[3] = ":red: You should not have let the gambling grow \n    out of control."
    when 'horse racing' then @story[3] = ":red:  Somebody spent way too much time at \n    the horse track."
    when 'poker' then @story[3] = ":red:  The cash total could have been less,\n    if SOMEBODY had a BETTER POKER FACE."
    when 'bar tab' then @story[3] = ":red:  Hoo hoo!  That's one large bar tab!!"
    when 'die' then @story[3] = ":red:If left untreated, DEADLY DYING DISEASE\n   almost always leads to DEATH."
    when 'constipation' then @story[3] = ":red:Without treatment, you will suffer from\n    HORRIFYING CONSTIPATION\n      which may lead to death."
    when 'body odor' then @story[3] = ":red:Untreated, the disease can cause\n  DISGUSTING BODY ODOR."
    when 'hair loss' then @story[3] = ":red:You may experience hair loss."
    when "..." then @story[3] = ""
    else @story[3] = ":red:No subreason."
    
  return @
