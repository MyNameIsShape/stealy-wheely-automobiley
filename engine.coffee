module.exports = {}
module.exports.terminal =
  cls: ()->
    process.stdout.write('\x1B[2J\x1B[0f\n')
  write: (value)->
    process.stdout.write value
  newline: ()->
    process.stdout.write "\n"
  writeline: (value)->
    process.stdout.write([value, '\n'].join(''))
  moveCursor: (x,y)->
    #`controlcode = "\033[<Y>,<X>H";`
    #controlcode = controlcode.replace "<X>", x.toString()
    #controlcode = controlcode.replace "<Y>", y.toString()
    #process.stdout.write controlcode
    #console.log controlcode
    `process.stdout.write("\033["+y+";"+x+"f");`
  write_line: (value)->
    process.stdout.write([value, '\n'].join(''))