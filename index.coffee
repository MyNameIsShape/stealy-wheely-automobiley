chalk = require 'chalk'
readline = require 'readline'
prompt = require 'prompt'
cli_cursor = require 'cli-cursor'

factory = require './factory.js'
engine = require './engine.js'
screenCards = require './screencards.js'
highscores = require './highscores.js'


cli_cursor.hide()

game = {}
game.clear_screen = false

game.world = {}
game.world.width = 10
game.world.height = 10

game.handlers = {}
game.handlers.keys = {}

game.cakeBits = 0

game.GetNameDraw = ()->
  pn = ''
  pn = game.PlayerName if game.PlayerName?
  engine.terminal.cls()
  engine.terminal.moveCursor 3,3
  engine.terminal.write "What's your name?"
  engine.terminal.moveCursor 4,4
  engine.terminal.write "> "
  engine.terminal.write pn

game.handlers.input = (str, key)->
  if game.gettingName?
    if key.name is "return"
      game.End()
      return
    if key.name is "backspace" and game.PlayerName.length > 0
      game.PlayerName = game.PlayerName.substring(0,game.PlayerName.length-1)
    if key.name != "backspace" and key.name != "delete" and key.name != "tab"
      k = key.name
      if key.shift is true
        k = key.sequence
      game.PlayerName = "#{game.PlayerName}#{k}"
    game.GetNameDraw()
    return
  if key.ctrl is true and key.name is "c"
    engine.terminal.cls()
    if not game.player?
      process.stdout.write(chalk.greenBright("\nThanks for playing!\n     Good-bye!\n\n"))
      process.exit()
    game.PlayerName = ''
    game.gettingName = true
    game.GetNameDraw()
    game.End = ()->
      highscores.set
        name: game.PlayerName
        score: game.player.cash
      engine.terminal.cls()
      process.stdout.write(chalk.greenBright("\nThanks for playing!\n     Good-bye!\n\n"))
      process.exit()
    return
  key_name = []
  key_name.push "ctrl " if key.ctrl is true
  key_name.push "shift " if key.shift is true
  key_name.push key.name
  key_name = key_name.join ''
      
  if game.handlers.keys[key_name]?
    game.handlers.keys[key_name]()
  else if game.handlers.keys["null"]?
    game.handlers.keys.null()
  else
    process.stdout.write(chalk.redBright([' > "', key_name, '" is an invalid selection!\x1B[K \n'].join('')))
    process.stdout.write('\x1B[1A')
    engine.terminal.moveCursor 12,23
    #process.stdout.write('\033[2D')
  return
    
playTime = 0
prevent_exit = ()->
  playTime += 1
  return
setInterval prevent_exit, 1000

readline.emitKeypressEvents(process.stdin);
process.stdin.setRawMode(true);
process.stdin.on('keypress', game.handlers.input)
engine.terminal.cls()
engine.terminal.moveCursor 8, 2

game.highscores = ()->
  score_list = ["","  -----------", chalk.greenBright("    Top Earners"),"  -----------",""]
  engine.terminal.cls()
  score_obj = highscores.get()
  for scorer_id of score_obj
    scorer = score_obj[scorer_id]
    n = Number(scorer_id)+1
    score_list.push [chalk.yellowBright(" #{n}."), chalk.greenBright(" $#{scorer.score}"), "  #{scorer.name}"].join ''
  #console.log score_obj
  #process.exit()
  score_list.push " "
  for scor_id of score_list
    engine.terminal.writeline score_list[scor_id]
  engine.terminal.writeline(["\n ",chalk.greenBright("(C)"),"ontinue"].join(""))
  game.handlers.keys["c"] = (()->
      game.handlers.keys = {}
      screenCards.title(game, engine, factory, chalk)
    )
    
screenCards.title(game, engine, factory, chalk)
game.handlers.keys["3"] = (()->
    screenCards.intro @game, @engine, @factory, @chalk
  ).bind({game:game, engine:engine, factory:factory, chalk:chalk})

game.drawmap = (params) ->
  map_offset = params.map_offset ?= {x: 14, y: 3}
  pos =
    x: 0
    y: 0
  map = params.map ?= null
  remapping = params.remapping ?= false
  if map is null
    return
  if remapping is false
    while pos.y < map.height
      engine.terminal.moveCursor pos.x+map_offset.x,pos.y+map_offset.y
      while pos.x < map.width
        coords = "x#{pos.x}y#{pos.y}"
        maptile = map.map[coords]
        switch typeof maptile
          when 'string'
            if map.map_paints? and map.map_paints[coords]?
              engine.terminal.write(map.map_paints[coords](maptile))
            else engine.terminal.write maptile
          else engine.terminal.write ' '
        pos.x++
      pos.x = 0
      pos.y++
  entitiesAt = []
  for entity_id of map.entities
    ent = map.entities[entity_id]
    if ent.visible? and ent.visible is false
      `continue`
    engine.terminal.moveCursor ent.x+map_offset.x, ent.y+map_offset.y
    if typeof ent.styling is 'function' then engine.terminal.write ent.styling ent.tile
    else engine.terminal.write ent.tile
    entitiesAt.push "x#{ent.x}y#{ent.y}"
    if remapping is true and ((ent.x != ent.xprevious) or (ent.y != ent.yprevious))
      previousLocation = "x#{ent.xprevious}y#{ent.yprevious}"
      if entitiesAt.includes(previousLocation) is false
        engine.terminal.moveCursor ent.xprevious+map_offset.x, ent.yprevious+map_offset.y
        if map.map_paints[previousLocation]? and typeof map.map_paints[previousLocation] is 'function'
          engine.terminal.write( map.map_paints[previousLocation](map.map[previousLocation]) )
        else engine.terminal.write map.map[previousLocation]
        
    ent.xprevious = ent.x
    ent.yprevious = ent.y
            
game.drawsidebar_left = ()->
  pos = 
    x: 0
    y: 5
  c = 
    x: pos.x
    y: pos.y
  for [1..12]
    engine.terminal.moveCursor c.x,c.y
    engine.terminal.write "             "
    c.y++
  hptotal = game.player.hp / game.player.hpMax
  hptotal *= 100
  hptotal = Math.floor(hptotal)
  expnextlvl = Math.floor(game.player.expNext - game.player.exp)
  
  w = Math.floor(game.player.wanted * game.player.wanted_rate)
  
  statbar = ["...Stats..."
             chalk.redBright " HP: #{hptotal}%  "
             chalk.greenBright "Cash: $ #{game.player.cash}"
             chalk.blueBright " Wanted:  #{w}"
             chalk.yellowBright " Next Lvl:"
             chalk.yellowBright "    #{expnextlvl} xp"
             ""
             chalk.bold " Equipment:"
             "Wpn:#{game.player.equipment.weapon}"
             "Tool: #{game.player.equipment.tool}"]
  for barline_id of statbar
    engine.terminal.moveCursor pos.x, pos.y
    barline = statbar[barline_id]
    engine.terminal.write barline
    #engine.terminal.write "\x1B[K"
    pos.y++

game.messages = []
game.draw_messages = (params)->
  params = params ?= {}
  pos = params.pos ?= 
    x: 45
    y: 18
  m = 0
  while m < game.messages.length
    msg = game.messages[m]
    if msg.length > 36
      msg_b = "  "+msg.substring 36
      game.messages[m] = msg.substring 0,36
      game.messages.splice m+1, 0, msg_b
      m = 0
    else
      m++
  
  while game.messages.length > 12
    game.messages.shift()
      
  m = game.messages.length-1
  while m >= 0
    engine.terminal.moveCursor pos.x, pos.y
    engine.terminal.write game.messages[m]
    engine.terminal.write "\x1B[K"
    pos.y--
    m--
  return

game.generate_map = (params)->
  params = params ?= {}
  params.type = params.type ?= "neighborhood"
  params.size = params.size ?= {width: 25, height: 15}
  params.map_pos = params.map_pos ?= {x: game.player.worldX, y: game.player.worldY}
  if params.type is 'neighborhood'
    map_coords = "x#{params.map_pos.x}y#{params.map_pos.y}"
    new_map = new factory.Map
      width: params.size.width
      height: params.size.height
      tiles: " ."
    new_map.initializeMap()
    
    new_map.solids += 'O║─'
    
    new_map.map_paints = {}
    
    new_map.name = factory.NewMapName()
    
    new_map.entities["cop0"] = new factory.Entity
      name: "Cop"
      x: (new_map.width-1)*Math.floor(Math.random()*2)
      y: Math.floor(new_map.height/2)+4
      solid: true
      tile: "C"
      cash: 100
      chat_message: 'Hey!'
      visible: false
    new_map.entities["cop0"].actions.act = ()->
      #game.messages.push "The cop is #{@countdown}"
      @visible = false
      if game.player.wanted*game.player.wanted_rate > 25
        if @visible is false
          game.messages.push "*\x1B[94cYou hear sirens in the\x1B[37c"
          game.messages.push "  \x1B[94cdistance...\x1B[37c"
      if game.player.wanted*game.player.wanted_rate >= 45
        if @countdown > 0
          @countdown--
          if game.player.wanted*game.player.wanted_rate >= 70
            @countdown-- 
          return false
      if @countdown <= 0
        @visible = true
        engine.terminal.write "**\x1B[91cThe COPS are CHASING you!!\x1B[37c"
        
        godistance = 1
        godistance++ if game.player.wanted*game.player.wanted_rate > 80
        godistance++ if game.player.wanted*game.player.wanted_rate > 128
        
        while godistance > 0
          d = 
            x:0
            y:0
          if game.player.x < @x then d.x += -1
          else if game.player.x > @x then d.x += 1
          else if game.player.y < @y then d.y += -1
          else if game.player.y > @y then d.y += 1
          if game.player.wanted*game.player.wanted_rate < 144
            @x += d.x
            @y += d.y
          else
            game.actions.move.bind(@)(d.x,d.y)
            
          cc = 
            x: Math.abs(@x-game.player.x)
            y: Math.abs(@y-game.player.y)
          if Math.abs(@x-game.player.x) <= d.x and Math.abs(@y-game.player.y) <= d.y
            engine.terminal.cls()
            game.PlayerName = ''
            game.gettingName = true
            game.GetNameDraw()
            game.End = ()->
              engine.terminal.cls()
              highscores.set
                name: game.PlayerName
                score: game.player.cash
              console.log(chalk.redBright("You got caught by the cops!"))
              console.log(chalk.redBright("  GAME OVER, man! GAME OVER!"))
              process.exit()
          godistance--
        return true
    
    new_map.entities["cop0"].actions.act = new_map.entities["cop0"].actions.act.bind new_map.entities["cop0"]
    new_map.street_tiles = "-"
    street_configs = ["horizontal", "vertical", "cross"]
    new_map.street_config = street_configs[Math.floor(Math.random()*street_configs.length)]
    new_map.street_config = "horizontal"
    switch new_map.street_config
      when 'horizontal'
        street_widths = [3,4,5]
        street_width = street_widths[Math.floor(Math.random()*street_widths.length)]
        new_map.street_positions = []
        new_street_pos = 
          x: 0
          y: Math.floor(new_map.height/2)-Math.floor(street_width/2)+2
          width: street_width
          type: "horizontal"
          tile: "-"
          styling: chalk.gray
        new_map.street_positions.push new_street_pos
      else
        return
    
    for street_info_id of new_map.street_positions
      street_info = new_map.street_positions[street_info_id]
      if street_info.type is "horizontal"
        sp = 
          x: street_info.x
          y: street_info.y
        while sp.y < street_info.y+street_info.width
          while sp.x < new_map.width
            new_map.map["x#{sp.x}y#{sp.y}"] = street_info.tile
            new_map.map_paints["x#{sp.x}y#{sp.y}"] = street_info.styling
            sp.x++
          sp.x = street_info.x
          sp.y++
    
    structure_counts = [4,5,6,7,8,9,10,11,12]
    structure_count = structure_counts[Math.floor(Math.random()*structure_counts.length)]
    while structure_count > 0
      building_type = "building"
      if building_type = "building"
        street = new_map.street_positions[Math.floor(Math.random()*new_map.street_positions.length)]
        p = 
          x: Math.floor(Math.random()*new_map.width)
          y: street.y
          width: Math.floor(Math.random()*8)+5
          height: Math.floor(Math.random()*4)+5
        p.y -= p.height+1
        if p.x+p.width >= new_map.width
          p.x = (new_map.width-1) - p.width
        if p.y+p.height >= new_map.height
          p.y = (new_map.height-1) - p.height
        
        can_place = true
        test_p = {x: p.x, y: p.y}
        while test_p.y < p.y+p.height
          while test_p.x < p.x+p.width
            if new_map.solids.includes new_map.map["x#{test_p.x}y#{test_p.y}"]
              can_place = false
              test_p = {x:1024,y:1024}
              break
            test_p.x++
          test_p.x = p.x
          test_p.y++
        draw_p = 
          x: p.x
          y: p.y
        if can_place is false
          structure_count-= 1
          `continue`
        while draw_p.y < p.y+ p.height
          while draw_p.x < p.x+p.width
            new_map.map["x#{draw_p.x}y#{draw_p.y}"] = "."
            new_map.map["x#{draw_p.x}y#{draw_p.y}"] = "║" if draw_p.x is p.x or draw_p.x is p.x+p.width-1 
            new_map.map["x#{draw_p.x}y#{draw_p.y}"] = "─" if draw_p.y is p.y or draw_p.y is p.y+p.height-1 
            if draw_p.x is p.x or draw_p.x is p.x+p.width-1
              if draw_p.y is p.y or draw_p.y is p.y+p.height-1
                new_map.map["x#{draw_p.x}y#{draw_p.y}"] = "O"
            new_map.map_paints["x#{draw_p.x}y#{draw_p.y}"] = chalk.gray
            draw_p.x++
          draw_p.x = p.x
          draw_p.y++
        building_name = factory.GenerateString('building name')
        e = new factory.Entity
          solid: false
          name: "sign#{structure_count}"
          x: p.x+Math.floor(Math.random()+p.width)
          y: p.y+p.height
          tile: "!"
          chat_message: "'#{building_name}'"
        e.shoptype = factory.ClassifyString building_name
        #game.messages.push "shoptype: #{e.shoptype}"
        
        e.cash = 8
        e.cash += Math.floor(Math.random()*42)
        e.actions['low'] = ()->
          if @x != game.player.x and @y != game.player.y
            return
          game.messages.push 'You look in the store window.'
        e.actions['high'] = ()->
          if @x != game.player.x and @y != game.player.y
            return
          if @cash <= 0
            game.messages.push "You've already robbed this"
            game.messages.push "  store. Also, you're"
            game.messages.push(chalk.redBright("RETURNING TO THE"))
            game.messages.push(chalk.redBright("  SCENE OF THE CRIME!!"))
            game.player.wanted++
            return
          game.messages.push 'You rob the store with'
          game.messages.push '  your FINGER GUNS!!'
          success_roll = Math.floor(Math.random()*20)+game.player.strength+game.player.smarts+game.player.luck
          difficulty = 10+game.player.wanted+@smarts
          if success_roll >= difficulty
            mm = ["They POOPED themselves!",
                  "Success!",
                  "Good job!!"]
            game.messages.push mm[Math.floor(Math.random()*mm.length)]
            game.messages.push(chalk.greenBright(" You obtained:"))
            game.messages.push(chalk.greenBright("  $ #{@cash}"))
            game.player.cash += @cash
            game.player.exp += Math.floor(@cash/3)
            @cash = 0
          else
            game.messages.push(chalk.redBright("UH OH!!!"))
            game.messages.push "  They shot at you!"
            game.messages.push "    You had to run away!"
            game.player.wanted++
            if Math.floor(Math.random()*100) < 15
              d = Math.floor(game.player.hpMax/3)
              dmg = d/game.player.hpMax
              dmg = Math.floor(dmg*100)
              game.player.hp -= d
              game.messages.push "You take"
              game.messages.push(chalk.redBright("  #{dmg}% HP damage!"))
              game.player.wanted+= 2
        
        if e.shoptype is 'food' 
          e.actions['low'] = ()->
            if @x != game.player.x and @y != game.player.y
              return
            game.messages.push(chalk.green("You buy some food."))
            game.player.hp += Math.floor(Math.random()*4)
            game.player.hp = game.player.hpMax if game.player.hp >= game.player.hpMax
            game.player.cash -= @cash
        else if e.shoptype is 'disguise'
          e.actions['low'] = ()->
            if @x != game.player.x and @y != game.player.y
              return
            msgs = ["You buy a fake mustache."
                    "You buy some fake glasses."
                    "You are now wearing a fake nose."
                    "You purchase a pair of slacks."]
            game.messages.push msgs[Math.floor(Math.random()*msgs.length)]
            game.player.cash -= @cash
            wanted_reduction = 1+Math.floor(@cash/7)
            game.player.wanted -= wanted_reduction
            game.player.wanted = 0 if game.player.wanted < 0
            e.cash = 8
            e.cash += Math.floor(Math.random()*42)
        
        else if e.shoptype is 'birthday'
          e.actions['low'] = ()->
            game.messages.push 'You get some free cake,'
            game.messages.push '  and some free money,'
            game.messages.push '   because of your birthday!'
            game.player.cash += @cash
            @cash = 0
            game.player.hp++
            if game.player.hp > game.player.hpMax
              game.player.hp = game.player.hpMax
        
        else if e.shoptype is 'tools'
          e.actions['low'] = ()->
            if game.player.equipment.tool != 'crowbar'
              game.messages.push 'You buy a crowbar.'
              game.player.equipment.tool = 'crowbar'
              game.player.cash -= @cash
              game.player.smarts += 4
            else
              game.messages.push 'you already have a crowbar...'
        
        else if e.shoptype is 'sports'
          e.actions['low'] = ()->
            if game.player.equipment.tool != 'baseball bat'
              game.messages.push 'You buy a baseball bat.'
              game.player.equipment.weapon = 'baseball bat'
              game.player.cash -= @cash
              game.player.strength += 4
            else
              game.messages.push 'you already have a baseball bat...'
        
        else if e.shoptype is 'job'
          e.actions['low'] = ()->
              if @cash <= 0
                game.messages.push "This job CANNOT PAY."
              @employed = @employed ?= false
              @fired = @fired ?= false
              if @employed is false
                game.messages.push "You apply for a job..."
                attempt = Math.floor(Math.random()*15)+game.player.smarts+Math.floor(Math.random()*game.player.luck)
                if game.player.wanted > 40
                  attempt = 0
                if attempt > 9
                  @employed = true
                  game.messages.push "\x1B[93cYOU GOT THE JOB!!\x1B[37"
                  @wage = Math.floor(@cash*0.8)
                  @payouts = 6+Math.floor(Math.random()*8)
                else
                  msg = 'You were "overqualified".'
                  if game.player.lvl < 4
                    msg = 'You are "inexperienced".'
                  if game.player.wanted > 40
                    msg = "You have a \x1B[91CRIMINAL RECORD.\x1B[37"
                  game.messages.push "You didn't get the job."
                  game.messages.push msg
              else if @employed is true and @fired is false
                game.player.cash += @wage
                @payouts -= 1
                game.messages.push "You are paid \x1B[92$#{@wage}\x1B[37c!"
                if @payouts <= 0
                  @fired = true
                  game.messages.push "You got fired."
                  msgs = ["You kept touching the customers.", "  For harrassment.", "  And replaced.","  Well darn."]
                  game.messages.push msgs[Math.floor(Math.random()*msgs.length)]
              if @fired is true
                game.messages.push "This job fired you!!"
                
        e.actions['low'] = e.actions['low'].bind e if e.actions['low']?
        e.actions['high'] = e.actions['high'].bind e if e.actions['high']?
        
              
              
        new_map.entities[e.name] = e
      structure_count--
    cars_count = 2+Math.floor(Math.random()*5)
    ci = 0
    street = new_map.street_positions[0]
    while cars_count > 0
      sides = ["top", "bottom"]
      side = sides[Math.floor(Math.random()*sides.length)]
      if side is 'top'
        cx = Math.floor(Math.random()*(new_map.width-1))
        cy = street.y
        can_place = true
        for e_id of new_map.entities
          e = new_map.entities[e_id]
          if not e?
            console.log new_map.entities
            process.exit()
          if e.x is cx+1 and e.y is cy
            can_place = false
            break
          if e.x is cx and e.y is cy
            can_place = false
            break
        if can_place is false
          `continue`
        car_front = new factory.Entity
          x: cx
          y: cy
          tile: '<'
          styling: chalk.magenta
          name: 'The car'
          car_id: ci
          solid: true
        new_map.entities["car#{ci}_front"] = car_front
        car_rear = new factory.Entity
          x: cx+1
          y: cy
          tile: '='
          styling: chalk.magenta
          name: 'The car'
          car_id: ci
          solid: true
        new_map.entities["car#{ci}_rear"] = car_rear
        car_front.dir = -1
        car_front.speed = 2
        car_front.lane_y = car_front.y
        car_front.rear = car_rear
        car_rear.front = car_front
        car_front.actions.high = game.actions.carjack
        car_front.actions.high = car_front.actions.high.bind car_front
        car_front.actions.low = game.actions.carjack_low
        car_front.actions.low = car_front.actions.low.bind car_front
      if side is 'bottom'
        cx = Math.floor(Math.random()*(new_map.width-1))
        cy = street.y+street.width-1
        can_place = true
        for e_id of new_map.entities
          e = new_map.entities[e_id]
          if e.x is cx+1 and e.y is cy
            can_place = false
            break
          if e.x is cx and e.y is cy
            can_place = false
            break
        if can_place is false
          `continue`
        car_rear = new factory.Entity
          x: cx
          y: cy
          tile: '='
          styling: chalk.magenta
          name: 'The car'
          car_id: ci
          solid: true
        new_map.entities["car#{ci}_rear"] = car_rear
        car_front = new factory.Entity
          x: cx+1
          y: cy
          tile: '>'
          styling: chalk.magenta
          name: 'The car'
          car_id: ci
          solid: true
        new_map.entities["car#{ci}_front"] = car_front
        car_front.dir = 1
        car_front.speed = 2
        car_front.lane_y = car_front.y
        car_front.rear = car_rear
        car_rear.front = car_front
        car_front.actions.high = game.actions.carjack
        car_front.actions.high = car_front.actions.high.bind car_front
        car_front.actions.low = game.actions.carjack_low
        car_front.actions.low = car_front.actions.low.bind car_front
      ci++
      cars_count--
    npc_counts = [4,4,4,4,4,4,4,6,6,6,5,5,7]
    npc_count = npc_counts[Math.floor(Math.random()*npc_counts.length)]
    ni = 0
    while npc_count > 0
      eid = "npc_#{ni}"
      new_map.entities[eid] = new factory.Entity
        name: "Bobby"
        x: Math.floor(Math.random()*new_map.width)
        y: Math.floor(Math.random()*new_map.height)
        solid: true
        tile: "B"
        cash: 100
        chat_message: 'Hey!'
      names = ["Bobby", "Jeremy", "Jimmy", "Kevin", "Kev", "Nathan", "Joey", "Vincent", "Paul", "Pops", "Derek", "Brock", "Ash", "Misty", "Gary", "Zach", "Martin"]
      new_map.entities[eid].name = names[Math.floor(Math.random()*names.length)]
      for e_i of new_map.entities
        ee = new_map.entities[e_i]
        if e_i != eid
          if ee.name == new_map.entities[eid].name
            new_map.entities[eid].name = names[Math.floor(Math.random()*names.length)] if Math.floor(Math.random()*100)> 20
      tile = new_map.entities[eid].name.charAt 0
      r = 31+Math.floor(Math.random()*7)
      tile = ["\x1B[#{r}m",tile,"\x1B[37m"].join ''
      new_map.entities[eid].tile = tile
      new_map.entities[eid].smarts = game.player.smarts
      add_smarts = Math.floor(Math.random()*10)-5
      new_map.entities[eid].smarts += add_smarts if Math.round(Math.random()*100) > 70
      cash_amounts = [20,20,20,20,20,30,30,30,50]
      new_map.entities[eid].cash = cash_amounts[Math.floor(Math.random()*cash_amounts.length)]
      new_map.entities[eid].cash *= 2 if Math.floor(Math.random()*100)>95
      new_map.entities[eid].cash = Math.floor(new_map.entities[eid].cash*1.25) if new_map.entities[eid].smarts > game.player.smarts
      
      new_map.entities[eid].actions["low"] = game.actions.pickpocket.bind new_map.entities[eid]
      new_map.entities[eid].actions["low"] = new_map.entities[eid].actions["low"].bind(new_map.entities[eid])
      new_map.entities[eid].actions["high"] = game.actions.mug.bind new_map.entities[eid]
      new_map.entities[eid].actions["high"] = new_map.entities[eid].actions['high'].bind new_map.entities[eid]
      new_map.entities[eid].actions.act = ()->
        move_now = true if Math.random()*100 > 66
        if move_now
          a = Math.random()*100
          game.actions.move.bind(@)(1,0) if a >= 70
          game.actions.move.bind(@)(-1,0) if a <= 30
          a = Math.random()*100
          game.actions.move.bind(@)(0,1) if a >= 70
          game.actions.move.bind(@)(0,-1) if a <= 30
      new_map.entities[eid].actions.act = new_map.entities[eid].actions.act.bind new_map.entities[eid]
      ni++
      npc_count--
  game.world[map_coords] = new_map
  return game.world[map_coords]  

game.start_new = ()->
  game.handlers.keys = {}
  #engine.terminal.writeline("..........") for[1..16]
  game.player = new factory.Entity()
  game.player.tile = "@"
  game.player.styling = chalk.blueBright
  game.player.cash = -3000
  game.player.x = 12
  game.player.y = 12
  game.player.wanted_rate = 1
                
  game.player.hpMax = 12+Math.floor(Math.random()*5)
  game.player.hp = game.player.hpMax
  
  game.action_mode 'explore default'
  game.key_mode 'explore default'
  new_map = game.generate_map
    map_pos:
      x: game.player.worldX
      y: game.player.worldY
    size:
      width: 25
      height: 15
    type: "neighborhood"
  
  new_map.entities["player"] = game.player
  #new_map.name = factory.NewMapName()
  
  game.world["x#{game.player.worldX}y#{game.player.worldY}"] = new_map
  game.clear_screen = true
  
  game.active = false
  
  game.messages.push "You arrive in:"
  game.messages.push [chalk.greenBright(new_map.name), "."].join ''
  
  game.explore_mode()
  game.active = true

game.action_mode = (mode)->
  if mode == 'explore default'
    game.actions = {}
    
    game.check_if_seen = (params)->
      params = params ?= {}
      params.distance = params.distance ?= 4
      seen_by = []
      for eid of game.map.entities
        ent = game.map.entities[eid]
        see = false
        if ent == @ or ent == @rear or ent == game.player or ent.name.toLowerCase().includes 'sign' or ent.name.toLowerCase() is 'cop' or ent.name.toLowerCase().includes 'car'
          see = false
        else
          see = true
        if see is true
          d = Math.floor(Math.sqrt(Math.pow((ent.x-@x),2)+Math.pow((ent.y-@y),2)))
          if d <= params.distance
            difficulty = 7
            see_roll = Math.floor(Math.random()*20)+@smarts+@luck+@lvl
            see = true
            see = false if see_roll < difficulty
            if see is true
              seen_by.push ent.name
              
      for ss of seen_by
        if seen_by[ss].includes 'car' or seen_by[ss].includes 'Cop'
          seen_by.splice(ss,1)
      if seen_by.length is 0
        game.messages.push "  Nobody saw you!"
      else if seen_by.length is 1
        game.messages.push "  #{seen_by[0]} saw you!"
        game.player.wanted++ if Math.random()*100 > 65
        game.player.wanted++ if Math.random()*100 > 85
      else if seen_by.length > 1
        game.messages.push "  \x1B[91mYou were seen by several\x1B[37m"
        game.messages.push "     \x1B[91mpeople!\x1B[37m"
        game.player.wanted += seen_by.length
        game.player.wanted++ if Math.random()*100 > 50
        game.player.wanted++ if Math.random()*100 > 50
    
    game.actions.carjack_low = ()->
        stolen = false
        tool = game.player.equipment.tool
        smarts_target_met = false
        if game.player.smarts >= 7
          smarts_target_met = true
        if smarts_target_met is false and tool is 'none'
          if not @car_locked?
            car_locked = Math.random()*100
            @car_locked = false
            @car_locked = true if car_locked < 80
          if @car_locked is true
            game.messages.push "Nice try, but the"
            game.messages.push "  car is locked."
          else
            game.messages.push "The car door was unlocked!"
            game.messages.push "  You open it!"
            stolen = true
        else if smarts_target_met is true and tool is 'none'
          if not @car_locked?
            car_locked = Math.random()*100
            @car_locked = false
            @car_locked = true if car_locked < 80
          if @car_locked is true
            game.messages.push "You manage to unlock the"
            game.messages.push "  car door, stealthily!"
            game.check_if_seen.bind(@)({distance:1})
            stolen = true            
          else
            game.messages.push "The car door was unlocked!"
            game.messages.push "  You open it!"
            stolen = true            
        else
          switch tool
            when 'crowbar'
              game.messages.push "You pry the door open with"
              game.messages.push "  your crowbar!"
              stolen = true            
              game.check_if_seen.bind(@)({distance:2})
            when 'jimmy'
              game.messages.push "You pop the lock open with"
              game.messages.push "  your jimmy!"
              game.check_if_seen.bind(@)({distance:1})
              stolen = true            
            else
              game.messages.push "You pry the door open with"
              game.messages.push "  your BARE HANDS!"
              stolen = true            
              game.check_if_seen.bind(@)({distance:2})
        if stolen is true
          @actions.high = game.actions.carjack_steal.bind @
          @actions.low = game.actions.carjack_steal.bind @
        
    game.actions.carjack = ()->
      if game.player.x != @x and game.player.y != @y
        game.messages.push "..."
        return
      messages = ['Using your shirt as a glove,', '  you smash the car window!']
      if game.player.smarts >= 4
        tool = game.player.equipment.tool.toLowerCase()
        switch tool
          when 'crowbar'
            messages = ['You pry the car door open', '  with your crowbar.']
          when 'jimmy'
            messages = ['You haphazardly pop the door', '  with your jimmy']
          when 'lockpicks'
            game.messages.push 'You have lockpicks.'
            game.messages.push '  Try being more low profile.'
            return
          else
            game.messages.push 'Why would you do that?'
            game.messages.push '  Use a little finesse!'
            return            
      game.messages.push "You smash the car's window!"
      
      game.check_if_seen.bind(@)()
        
      @actions.high = game.actions.carjack_steal.bind @
      @actions.low = game.actions.carjack_steal.bind @
    
    game.actions.carjack_steal = ()->
      game.messages.push "You steal the car!"
      game.check_if_seen.bind(@)()
      
      game.player.x = @x
      game.player.y = @y
      
      game.drawmap
        map: game.map
        map_offset:
          x: 14
          y: 3
        remapping: true
      
      game.handler_bank_keys = game.handlers.keys
      game.handlers.keys = {}
      game.player.driving_profile = "high"
      drive_function_high = ()->
        #game.messages.push "(High Profile)"
        outcome_goodness = Math.random()*100
        outcome_goodness_skew = 1
        if game.player.smarts <= 3
          outcome_goodness_skew = 0.8
        else if game.player.smarts >= 6 and game.player.smarts <= 10
          outcome_goodness_skew = 1.2
        else if game.player.smarts > 10
          outcome_goodness_skew = 1.4
        outcome_goodness *= outcome_goodness_skew
        if outcome_goodness >= 80
          game.messages.push "You drive REALLY FAST!"
        else if outcome_goodness >= 40
          game.messages.push "You drive recklessly,"
          game.messages.push "  with complete disregard"
          game.messages.push "    for the rules of the road!"
          game.messages.push "You hop curbs and hit cars!"
          dmg = 1+Math.floor(Math.random()*(2-(game.player.luck/3)))
          dmg = 1 if dmg < 1
          damage = Math.floor((dmg/game.player.hpMax)*100)
          game.messages.push "  \x1B[31m#{damage}% HP dmg!\x1B[37m"
          game.player.hp -= dmg
          @x += (@speed+1)*@dir
          @y = @lane_y
          game.player.wanted++
        else
          game.messages.push "You HOP THE CURB"
          game.messages.push "  and DRIVE down the SIDEWALK!"
          @x += @dir
          @y = @lane_y + @dir
          for ent_id of game.map.entities
            ent = game.map.entities[ent_id]
            if ent.lane_y?
              `continue`
            if ent != @ and ent != @rear and ent != game.player and ((ent.x == @x and ent.y == @y) or (ent.x == @rear.x and ent.y == @rear.y))
              if ent.name.includes 'sign'
                game.messages.push "You run over the sign for"
                game.messages.push "  #{ent.chat_message}!"
                game.player.wanted++
              else
                game.messages.push "You run over #{ent.name},"
                game.messages.push "  \x1B[31cKILLING THEM\x1B[37c!!"
                ent.actions = {}
                ent.cash = 0
                ent.tile = ent.name.charAt(0).toLowerCase()
                game.player.wanted += (7+Math.floor(Math.random(7)))
            else
              `continue`
        game.explore_mode
          remap: true
      keys_to_map = ["w","a","s","d","f","g"]
      for kid of keys_to_map
        game.handlers.keys[keys_to_map[kid]] = drive_function_high.bind @
      @actions.act = ()->
        #game.actions.move.bind(@)(@dir,0)
        @x+=@dir*@speed
        game.player.x = @x
        game.player.y = @y
        @rear.x = @x-@dir
        @rear.y = @y
        
        if @x < 0 or @x >= game.map.width
          game.handlers.keys = {}
          
          lines = []
          game.player.wanted += 3
          
          sale_total = game.map.car_sale_price
          sale_bonus = 0
          
          exp_total = Math.floor(sale_total/15)
          
          crits = Math.floor(Math.random()*20)+1
          if crits <= 2
            lines.push "  OH NO!"
            lines.push "     You stole a \x1B[31cHOT CAR\x1B[37c!!"
            lines.push "  Your wanted level has increased significantly!!!"
            game.player.wanted += 5+Math.floor(Math.random()*25)
            exp_total = Math.floor(exp_total*2.5)
            
          if crits >= 19
            lines.push(chalk.green("    CRITICAL SUCCESS!"))
            sale_total += game.map.car_sale_bonus
            sale_bonus += game.map.car_sale_bonus
          
          crits += game.player.smarts + game.player.luck
          if crits > 15
            sale_total += game.map.car_sale_bonus
            sale_bonus += game.map.car_sale_bonus
            saleslines = ["You got a bonus",chalk.greenBright("  $ #{sale_bonus}")]
            extra_sale_messages = ["    from selling the sound system."]
            extra_sale_messages.push "    from the SWEET RIMS."
            extra_sale_messages.push "    because you're savvy!" if game.player.smarts > 3
            extra_sale_messages.push "    because it's a luxury model."
            saleslines.push extra_sale_messages[Math.floor(Math.random()*extra_sale_messages.length)]
            for ll of saleslines
              lines.push saleslines[ll]
              
          game.player.cash += game.map.car_sale_price
          game.player.cash += sale_bonus
          game.player.exp += exp_total
          
          for ent_id of game.map.entities
            ent = game.map.entities[ent_id]
            if ent == @rear or ent == @
              delete game.map.entities[ent_id]
          
          engine.terminal.cls()
          engine.terminal.moveCursor 8,3
          lines.push "The #{game.map.name} chop shop pays you"
          lines.push ["   ", chalk.greenBright("$ #{sale_total}"), chalk.white('')].join ''
          lines.push "        for the car."
          lines.push ""
          lines.push(chalk.yellowBright("  You gain #{exp_total} exp."))
          lines.push ""
          lines.push(chalk.greenBright("Press (C) to continue"))
          lp = 
            x:8
            y:3
          for lid of lines
            engine.terminal.moveCursor lp.x,lp.y
            engine.terminal.write lines[lid]
            lp.y++
          game.handlers.keys['c'] = ()->
            game.handlers.keys = game.handler_bank_keys
            game.player.x = 12
            game.player.y = 12
            game.player.worldX = @newWorldX
            if game.map.entities['player']?
              delete game.map.entities['player']
            
            if not game.world["x#{game.player.worldX}y#{game.player.worldY}"]?
              new_map = game.generate_map
                map_pos:
                  x: game.player.worldX
                  y: game.player.worldY
                size:
                  width: 25
                  height: 15
                type: "neighborhood"
              game.world["x#{game.player.worldX}y#{game.player.worldY}"] = new_map
            game.map = game.world["x#{game.player.worldX}y#{game.player.worldY}"]
            game.map.entities["player"] = game.player
            #new_map.name = factory.NewMapName()
            game.clear_screen = true
            game.active = false

            game.messages.push "You arrive in:"
            game.messages.push [chalk.greenBright(game.map.name), "."].join ''

            game.explore_mode()
            game.active = true            
          nx = game.player.worldX+@dir
          nx = 0 if nx >= 10
          nx = 9 if nx < 0
          game.handlers.keys['c'] = game.handlers.keys['c'].bind {newWorldX: nx}
          return 'interrupt'
        
      @actions.act = @actions.act.bind @
      
    
    game.actions.mug = ()->
      if @cash <= 0
        game.messages.push "#{@name} has nothing to steal..."
        return
      game.messages.push "You try to mug #{@name}..."
      attempt = Math.floor(Math.random()*21)+game.player.smarts+game.player.luck+game.player.strength
      difficulty = 15+@smarts+@speed+@strength
      difficulty += 3 if Math.random()*100 > 70
      difficulty += 3 if Math.random()*100 > 70
      difficulty += 2 if Math.random()*100 > 70
      game.messages.push "#{attempt} vs. #{difficulty}, #{@smarts} #{@luck} #{@speed} #{@strength}"
      success = false
      success = true if attempt >= difficulty
      if success is true
        move_amount =
          x: 0
          y: 0
        move_amount.x = 1 if game.player.x < @x
        move_amount.x = -1 if game.player.x > @x
        move_amount.y = 1 if game.player.y < @y
        move_amount.y = -1 if game.player.y > @y
        game.actions.move.bind(@)(move_amount.x, move_amount.y)
        game.actions.move.bind(@)(move_amount.x, move_amount.y)
        game.messages.push "  #{@name} is running scared!"
        game.messages.push "  You get:"
        game.messages.push(chalk.greenBright("   $ #{@cash} !"))
        game.player.cash += @cash
        @cash = 0
        game.player.wanted++
        game.player.wanted++ if Math.random()*100 > 75
        game.player.wanted++ if Math.random()*100 > 90
        return
      else
         move_amount =
          x: 0
          y: 0
        move_amount.x = -1 if game.player.x < @x
        move_amount.x = 1 if game.player.x > @x
        move_amount.y = -1 if game.player.y < @y
        move_amount.y = 1 if game.player.y > @y
        
        damage = Math.floor(Math.random()*@strength)
        if damage <= 0
          damage = 1
        damage_percent = Math.floor((damage/game.player.hpMax)*100)
        distances = []
        distances.push 1 for [1..8]
        distances.push 2
        distances.push 3
        d = distances[Math.floor(Math.random()*distances.length)]
        while d > 0
          game.actions.move.bind(game.player)(move_amount.x, move_amount.y)
          d--
        game.messages.push "  #{@name} fights back!"
        game.messages.push "  You take:"
        game.messages.push(chalk.redBright("   #{damage_percent}% dmg!"))
        
        game.player.hp -= damage
        
        game.player.wanted++
        game.player.wanted++ if Math.random()*100 > 75
        game.player.wanted++ if Math.random()*100 > 90
        return
    
    game.actions['pickpocket'] = ()->
      game.messages.push "You try to pick #{@name}'s pocket..."
      attempt = Math.floor(Math.random()*21)+game.player.speed+game.player.smarts+game.player.luck
      if attempt > 10+@speed+@luck
        if @cash > 0
          grabbed = Math.floor(Math.random()*@cash)
          if grabbed <= 0 then grabbed = 1
          game.messages.push "You successfully grabbed:"
          game.messages.push(chalk.greenBright("  $ #{grabbed} !!"))
          game.player.cash += grabbed
          expgain = Math.floor(grabbed/5)
          if expgain <= 0 then expgain = 1
          game.player.exp += expgain
          game.messages.push(chalk.yellowBright("You gain #{expgain}xp!"))
          @cash -= grabbed
        else
          game.messages.push "#{@name} has nothing to steal."
      else
        game.messages.push(chalk.redBright("  YOU GOT CAUGHT!"))
        game.messages.push "#{@name} attacks you!"
        dmg = 1+Math.floor(Math.random()*(@strength+1))
        damage = Math.floor((dmg / game.player.hpMax)*100)
        game.player.hp -= dmg
        game.messages.push(chalk.redBright("  You lose #{damage}% HP"))
        game.player.wanted++

    game.actions['move'] = (x,y)->
      return_data = {success: false}
      newpos = 
        x: @x+x
        y: @y+y
      if newpos.x < 0 or newpos.x >= game.map.width or newpos.y < 0 or newpos.y >= game.map.height
        return_data.result = "bounds"
        return_data.msg = "You can't go that way."
        return return_data
      mapTerrain = game.map.map["x#{newpos.x}y#{newpos.y}"]
      if game.map.solids.includes mapTerrain
        return_data.result = "tile"
        return return_data
      for ent_id of game.map.entities
        ent = game.map.entities[ent_id]
        if ent == @
          `continue`
        else if ent.x is newpos.x and ent.y is newpos.y and ent.solid is true
          return_data =
            success: false
            result: 'entity'
            msg: "#{ent.name} is in the way."
          if ent.chat_message?
            return_data.msg = ent.chat_message
          return return_data
      return_data = {success: true}
      @x += x
      @y += y
      return return_data
    game.player.actions.checkSigns = ()->
      for e_id of game.map.entities
        e = game.map.entities[e_id]
        if e.x == @x and e.y == @y
          if e.chat_message?
            game.messages.push e.chat_message
    game.player.actions.checkSigns = game.player.actions.checkSigns.bind game.player
    game.player.actions.move = game.actions.move.bind game.player
    game.player.actions['north'] = ()->
      m = game.player.actions.move 0, -1
      if m.success is false
        if m.msg? then game.messages.push m.msg
        else game.messages.push "You can't go that way."
      else
        game.player.actions.checkSigns()
    game.player.actions['west'] = ()->
      m = game.player.actions.move -1, 0
      if m.success is false
        if m.msg? then game.messages.push m.msg
        else game.messages.push "You can't go that way."
      else
        game.player.actions.checkSigns()
    game.player.actions['south'] = ()->
      m = game.player.actions.move 0, 1
      if m.success is false
        if m.msg? then game.messages.push m.msg
        else game.messages.push "You can't go that way."
      else
        game.player.actions.checkSigns()
    game.player.actions['east'] = ()->
      m = game.player.actions.move 1, 0
      if m.success is false
        if m.msg? then game.messages.push m.msg
        else game.messages.push "You can't go that way."
      else
        game.player.actions.checkSigns()
  else
    game.player.actions = {}

game.key_mode = (mode)->
  game.handlers.keys = {}
  if mode == 'explore default'      
    game.handlers.keys["w"] = ()->
      game.player.actions.north()
      game.explore_mode
        remap: true
    game.handlers.keys["a"] = ()->
      game.player.actions.west()
      game.explore_mode
        remap: true
    game.handlers.keys["s"] = ()->
      game.player.actions.south()
      game.explore_mode
        remap: true
    game.handlers.keys["d"] = ()->
      game.player.actions.east()
      game.explore_mode
        remap: true
    game.handlers.keys["f"] = ()->
      found = false
      find_action = "low"
      for e_id of game.map.entities
        e = game.map.entities[e_id]
        if Math.abs(e.x-game.player.x) <= 1 and Math.abs(e.y-game.player.y) <= 1
          if e.actions[find_action]?
            e.actions[find_action]()
            found = true
            break
      if found is false
        game.messages.push "You wait..."
      game.explore_mode
        remap: true
    game.handlers.keys["g"] = ()->
      found = false
      find_action = "high"
      for e_id of game.map.entities
        e = game.map.entities[e_id]
        if Math.abs(e.x-game.player.x) <= 1 and Math.abs(e.y-game.player.y) <= 1
          if e.actions[find_action]?
            e.actions[find_action]()
            found = true
            break
      if found is false
        game.messages.push "You shout!"
      game.explore_mode
        map:true
        remap: false
  else
    game.handlers.keys['c'] = ()->
      process.exit()
   
game.explore_mode = (params)->
  params = params ?= {}
  params.map = params.map ?= true
  params.sidebar_left = params.sidebar_left ?= true
  params.remap = params.remap ?= false
  
  game.clear_screen = params.clear_screen ?= game.clear_screen
  
  params.active = params.active ?= game.active ?= false
  
  game.map = game.world["x#{game.player.worldX}y#{game.player.worldY}"]
  map = game.map
  
  if game.player.hp <= 0
    game.messages.push 'You aren\'t feeling so good.. '
  if game.player.hp < -2
    engine.terminal.cls()
    game.PlayerName = ''
    game.gettingName = true
    game.GetNameDraw()
    game.End = ()->
      highscores.set
        name: game.PlayerName
        score: game.player.cash
      engine.terminal.cls()
      engine.terminal.write "\x1B[31cYou have died.\n  Better luck next time.\x1B[37c\n"
      process.exit()
  
  if game.player.exp >= game.player.expNext
    game.messages.push chalk.yellowBright "You have leveled up!"
    game.player.lvl++
    game.player.strength += Math.floor(Math.random()*3)+1
    game.player.smarts += Math.floor(Math.random()*3)+1
    game.player.speed += Math.floor(Math.random()*3)+1
    game.player.luck += Math.floor(Math.random()*3)+1
    game.player.wanted += game.player.lvl
    game.player.exp = 0
    game.player.expNext = Math.floor(game.player.expNext*1.75)
    game.player.hpMax += 1+Math.floor(Math.random()*((game.player.strength+game.player.luck)/3))
    game.player.hp = game.player.hpMax
    
  #map.name = factory.NewMapName()
    
  if params.active? and params.active is true
    for ent_id of map.entities
      ent = map.entities[ent_id]
      if ent.actions.act? and typeof ent.actions.act is 'function'
        r = ent.actions.act()
        if r is "interrupt"
          return
  
  if game.clear_screen is true
    game.clear_screen = false
    engine.terminal.cls()
  
            
  #Print Sidebars
  game.drawsidebar_left() if params.sidebar_left is true
  
  engine.terminal.moveCursor 0, 0
  engine.terminal.write chalk.cyanBright "Stealy Wheely Automobiley v1.0.2"
  engine.terminal.moveCursor 0,2
  engine.terminal.write(chalk.bold("You are in:"))
  mnp = {x:2,y:3}
  engine.terminal.moveCursor 2,3
  engine.terminal.write "            "
  engine.terminal.moveCursor 2,4
  engine.terminal.write "            "
  engine.terminal.moveCursor 2,3
  map_name_lines = [map.name]
  if map_name_lines[0].length > 12
    map_name_lines.push(map_name_lines[0].substring(map_name_lines[0].indexOf(' ')))
    map_name_lines[0] = map_name_lines[0].substring(0,map_name_lines[0].indexOf(' '))
  for mn in map_name_lines
    engine.terminal.moveCursor mnp.x, mnp.y
    engine.terminal.write(chalk.bold.greenBright(mn))
    mnp.y++
  #print map
  if params.map is true
    game.drawmap
      map: map
      map_offset:
        x: 14
        y: 3
      remapping: params.remap
  
  lines = [ ["(", chalk.greenBright("WASD"), "). North"].join ''
            ["(", chalk.greenBright("F"), "). Action (Low Profile)"].join ''
            ["(", chalk.greenBright("G"), "). Action (High Profile)"].join '']
  px = 1
  py = 18
  for l in lines
    engine.terminal.moveCursor px, py
    engine.terminal.write l
    py++
  
  engine.terminal.moveCursor 12,22
  engine.terminal.write chalk.greenBright "> Enter a command. <\n"
  engine.terminal.write "\x1B[?16;0;16c"
  
  game.draw_messages()
  engine.terminal.moveCursor 12,23
  engine.terminal.write "\x1B[?5c"
  if game.player.wanted > 25
    game.player.wanted_rate += 0.000125
  engine.terminal.write "#{game.player.wanted_rate}"
  #engine.terminal.moveCursor 36, 7
  #console.log game.messages
    
game.close = ()->
  game.handlers.input {},{ctrl: true, name: "c"}
